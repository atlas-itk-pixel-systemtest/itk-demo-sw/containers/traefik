# Portainer and Traefik for DeMi

This repository contains a maintained `docker compose` file (DeMi "stack") for 
the reverse proxy Traefik.

[Traefik](https://doc.traefik.io/traefik/) is a special case because 
it can be used to reverse proxy all other containers.

Therefore it is started system-wide.

Use remains optional. You can just use docker compose and/or pydemi.

Standard scripts to bootstrap Docker and write the required environment variables 
to `.env` for site-wide shared tools needed by DeMi microservices are provided.

Usage:

```shell
./bootstrap
./config
docker compose up -d
```

- `bootstrap` this script sets up all dependencies needed to bring up the containers.
- `config` this script writes the `.env` configuration.


---
Contact: G. Brandt <gbrandt@cern.ch>

